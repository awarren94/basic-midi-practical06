/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"


//==============================================================================
MainComponent::MainComponent() 
{
    setSize (500, 400);
    
    /**Print available midi devices */
    StringArray sa (MidiInput::getDevices());
    for (String s : sa)
        DBG (s);
    
    audioDeviceManager.setMidiInputEnabled("USB Axiom 49 Port 1", true);
    audioDeviceManager.addMidiInputCallback (String::empty, this);
    
    addAndMakeVisible(midiLabel);
    midiLabel.setText(String::empty, dontSendNotification);
    audioDeviceManager.setDefaultMidiOutput("SimpleSynth virtual input");
    
    addAndMakeVisible(channelSlider);
    channelSlider.setSliderStyle(Slider::IncDecButtons);
    channelSlider.setRange(1, 16, 1);
    addAndMakeVisible(channelLabel);
    channelLabel.setText("Channel", dontSendNotification);
    
    addAndMakeVisible(numberLabel);
    addAndMakeVisible(numberSlider);
    numberSlider.setSliderStyle(Slider::IncDecButtons);
    numberSlider.setRange(0, 127, 1);
    numberLabel.setText("Number", dontSendNotification);
    
    addAndMakeVisible(velocityLabel);
    addAndMakeVisible(velocitySlider);
    velocitySlider.setSliderStyle(Slider::IncDecButtons);
    velocitySlider.setRange(0, 127, 1);
    velocityLabel.setText("Velocity", dontSendNotification);
    
    addAndMakeVisible(controlSlider);
    addAndMakeVisible(controlLabel);
    controlSlider.setSliderStyle(Slider::IncDecButtons);
    controlSlider.setRange(0, 127, 1);
    controlLabel.setText("Control Value", dontSendNotification);
}

MainComponent::~MainComponent()
{
    audioDeviceManager.removeMidiInputCallback (String::empty, this);
   
    
}

void MainComponent::resized()
{
    // midiLabel.setBounds(0,0,getWidth(),getHeight());
    channelSlider.setBounds(10, 30, 130, 25);
    channelLabel.attachToComponent(&channelSlider, false);
    
    numberSlider.setBounds(10, 90, 130, 25);
    numberLabel.attachToComponent(&numberSlider, false);
    
    velocitySlider.setBounds(10, 160, 130, 25);
    velocityLabel.attachToComponent(&velocitySlider, false);
    
    controlSlider.setBounds(10, 220, 130, 25);
    controlLabel.attachToComponent(&controlSlider, false);
    
}

void MainComponent::handleIncomingMidiMessage (MidiInput*, const MidiMessage &message)
{
    DBG("MIDI\n");
    
    String midiText;
    
    
        MidiMessage::setChannel(channelLabel);
        midiText << " :Number" << message.getNoteNumber();
        midiText << " :Velocity" << message.getVelocity();
    }
    else if (message.isController())
    {
        midiText <<"\n\ncontroller value: " << message.getControllerValue();
    }
    else if (message.isPitchWheel())
    {
        midiText << "Pitch bend value: " << message.getPitchWheelValue();
    }
    
    else if (message.isAftertouch())
    {
    midiText << "Aftertouch: " << message.getAfterTouchValue();
    }
    
    midiLabel.getTextValue() = midiText;
    
    audioDeviceManager.getDefaultMidiOutput()->sendMessageNow(message);
}
